from django.apps import AppConfig


class BoxesConfig(AppConfig):
    name = "apps.boxes"
