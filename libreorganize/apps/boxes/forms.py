from django import forms
from django.utils.translation import gettext_lazy as _
from django_ckeditor_5.fields import CKEditor5Widget

from apps.boxes.models import Box, Upload


class BoxForm(forms.ModelForm):
    class Meta:
        model = Box
        fields = "__all__"
        widgets = {"content": CKEditor5Widget(config_name="box")}


class UploadForm(forms.ModelForm):
    class Meta:
        model = Upload
        fields = ("file",)
