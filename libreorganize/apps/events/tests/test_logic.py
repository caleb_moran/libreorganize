from django.core.management import call_command
from django.test import TestCase
from apps.accounts.models import Account
from apps.events.models import Event
import datetime
from dateutil.relativedelta import relativedelta


class TestCalandarView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_months(self):
        """Check to make sure user can change months"""
        response = self.client.get("/events/calendar/?month=2020-4")
        self.assertContains(response, "April 2020")
        response = self.client.get("/events/calendar/?month=2020-5")
        self.assertContains(response, "May 2020")

    def test_month(self):
        """Check to make sure user can change months"""
        response = self.client.get("/events/calendar/")
        now = datetime.datetime.now()
        self.assertContains(response, now.strftime("%B %Y"))


class TestListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)


class TestCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_end_date_before(self):
        """Check end date before start date (date)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-02-01 9:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The start date must be less than the end date.")

    def test_calendar_create(self):
        """Test Calendar Date Autofill"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/?start_date=1588780800",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-05-06 12:00",
                "end_date": "2020-05-06 13:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        response = self.client.get("/events/")
        self.assertContains(response, "May 06, 2020, 12:00 PM")

    def test_max_part_less_than_neg_one(self):
        """Max Participants Less than -1"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-3",
            },
            follow=True,
        )
        self.assertContains(response, "The maximum number of participants must be higher than -1.")

    def test_successful_weekly_event(self):
        """Check successful weekly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.get("/events/calendar/?month=2020-3")
        self.assertContains(response, "Test")

    def test_successful_monthly_event(self):
        """Check successful montly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "monthly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")

    def test_successful_yearly_event(self):
        """Check successful yearly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")

    def test_weekly_event(self):
        """Event is longer than one week and is marked as weekly"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-09 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 7 days if it reoccurs weekly.")

    def test_monthly_event(self):
        """Event is longer than one month and is marked as monthy"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "monthly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 28 days if it reoccurs monthly.")

    def test_yearly_event(self):
        """Event is longer than one year and is marked as yearly"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 365 days if it reoccurs yearly.")

    def test_occurrences_bigger_than(self):
        """Occurrences is larger than 12"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "100",
            },
            follow=True,
        )
        self.assertContains(response, "The number of occurrences must be between 1 and 12.")

    def test_occurrences_smaller_than(self):
        """Occurrences is smaller than 1"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "0",
            },
            follow=True,
        )
        self.assertContains(response, "The number of occurrences must be between 1 and 12.")

    def test_occurrences(self):
        """Occurrences successful occurences"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")

    def test_successful_edit_recur_start_end(self):
        """Check successful edit for event and applying it to future events"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "5",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.post(
            "/events/3/edit/",
            {
                "title": "New Title",
                "description": "This is a test",
                "start_date": "2020-03-02 11:45",
                "end_date": "2020-03-02 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "modalRadio": "2",
            },
            follow=True,
        )
        self.assertContains(response, "New Title")

    def test_successful_edit_recur_all(self):
        """Check successful edit for event and applying it to all events"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "5",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.post(
            "/events/5/edit/",
            {
                "title": "New Title",
                "description": "This is a test",
                "start_date": "2020-03-02 11:45",
                "end_date": "2020-03-02 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "modalRadio": "3",
            },
            follow=True,
        )
        self.assertContains(response, "New Title")


class TestDetailView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_does_not_exist(self):
        """Check if Event Exists"""
        response = self.client.get("/events/100/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_detail_page(self):
        """Bring to Detail Page"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/", follow=True)
        self.assertContains(response, "Start Date")

    def test_private_event(self):
        """Test private event"""
        response = self.client.get("/events/1/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_end_date_before(self):
        """Check end date before start date (date)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-02-01 9:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The start date must be less than the end date.")


class TestEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_does_not_exist_edit(self):
        """Check if Event Exists Edit"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/edit/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_does_exist(self):
        """Event Exists bring to details"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/edit/", follow=True)
        self.assertContains(response, "Start Date")

    def test_edit_event(self):
        """edit event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 14:45",
                "end_date": "2020-03-01 15:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully edited.")
        self.assertRedirects(response, "/events/")

    def test_max_part_less_than_neg_one_edit(self):
        """Max Participants Less than -1 Edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-100",
            },
            follow=True,
        )
        self.assertContains(response, "The maximum number of participants must be higher than -1.")


class TestDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_incorrect_permissions_delete(self):
        """Incorrect Permissions to Delete"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/delete/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.assertRedirects(response, "/")

    def test_event_does_not_exist_delete(self):
        """Check if Event Exists Delete"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/delete/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_delete(self):
        """Check delete"""
        self.client.force_login(self.superuser)
        response = self.client.post("/events/1/delete/", follow=True)
        self.assertContains(response, "The event has been deleted.")

    def test_successful_delete(self):
        """Check successful delete for event and deleting all events"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "5",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.post("/events/5/delete/", {"modalRadio": "1"}, follow=True,)
        self.assertNotContains(response, "2020-03-15 11:45")
        response = self.client.post("/events/7/delete/", {"modalRadio": "1"}, follow=True,)
        self.assertNotContains(response, "2020-03-29 11:45")

    def test_successful_delete_future(self):
        """Check successful delete for event and deleting all future events"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "5",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.post("/events/5/delete/", {"modalRadio": "2"}, follow=True,)
        self.assertNotContains(response, "2020-03-15 11:45")


class TestCheckView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_does_not_exist_check(self):
        """Check if Event Exists Check"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/check/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_check_in(self):
        """Check in & out"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": start,
                "end_date": end,
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        response = self.client.get("/events/3/check/", follow=True)
        self.assertContains(response, "You checked in.")
        response = self.client.get("/events/3/check/", follow=True)
        self.assertContains(response, "You have already checked in!")

    def test_max_participant(self):
        """No more participants allowed"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": start,
                "end_date": end,
                "location": "1234 Washington Blvd",
                "max_participants": "1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        response = self.client.get("/events/3/check/", follow=True)
        self.assertContains(response, "You checked in.")
        self.client.force_login(self.user)
        response = self.client.get("/events/3/check/", follow=True)
        self.assertContains(response, "There are no more empty seats for this event!")

    def test_cant_checkin(self):
        """No more participants allowed"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "9999-05-02 11:24",
                "end_date": "9999-05-02 11:25",
                "location": "1234 Washington Blvd",
                "max_participants": "1",
                "frequency": "weekly",
                "occurrences": "1",
            },
            follow=True,
        )
        response = self.client.get("/events/3/check/", follow=True)
        self.assertContains(response, "You can only check in 24 hours before and after an event.")


class TestSubscribeUtil(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Setup accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_subscribe(self):
        """Test download subscribe"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/subscribe", follow=True)


class CommandsTestCase(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Setup accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)

    def test_successful(self):
        """Test successful weekly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": datetime.datetime.now() + relativedelta(hours=1),
                "end_date": datetime.datetime.now() + relativedelta(hours=2),
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",

            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        call_command("update_events_notification")
        response = self.client.get("/events/2/", follow=True)
