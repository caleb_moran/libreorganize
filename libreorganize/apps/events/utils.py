import datetime
import calendar

from django.conf import settings
from django.urls import reverse
from django.utils.safestring import mark_safe
from django_ical.views import ICalFeed

from apps.events.models import Event


class Calendar(calendar.HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super().__init__(firstweekday=settings.FIRST_WEEKDAY)

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events, year, month):
        events_per_day = events.filter(start_date__day=day).order_by("start_date")
        d = ""
        for event in events_per_day:
            d += f"<a class='calendar-event' href='{reverse('events:detail', args={event.uid})}'>{event.start_date.strftime('%I:%M%p')} | {event.title}</a> <a class='calendar-event-mobile' href='{reverse('events:detail', args={event.uid})}'><i class='fas fa-dot-circle'></i></a>"

        if day != 0:
            scope = datetime.date(year, month, day)
            start_time = int(datetime.datetime(year=year, month=month, day=day, hour=12, minute=0).timestamp())
            day_class = 'class="day-table"'
            if scope == datetime.date.today():
                day_class = 'class="date-today"'
            return f"<div id='content'><td {day_class}><span class='date'>{day}</span><div> {d} <a class='perms.events.create_events button btn btn-sm btn-success' href='/events/create/?start_date={start_time}'>Create Event</a></div></td></div>"
        return "<td></td>"

    # formats a week as a tr
    def formatweek(self, theweek, events, year, month):
        week = ""
        for d, weekday in theweek:
            week += self.formatday(d, events, year, month)
        return f"<tr> {week} </tr>"

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, withyear=True, privateevents=False):
        if privateevents:
            events = Event.objects.filter(start_date__year=self.year, start_date__month=self.month)
        else:
            events = Event.objects.filter(start_date__year=self.year, start_date__month=self.month, is_private=False)

        cal = (
            f'<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-bordered calendar">\n'
        )
        # cal += f"{self.formatmonthname(self.year, self.month, withyear=withyear)}\n"
        cal += f"{self.formatweekheader()}\n"
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f"{self.formatweek(week, events, self.year, self.month)}\n"
        cal += "</table>"
        return mark_safe(cal)


class EventFeed(ICalFeed):
    """Map calendar events to iCal attributes"""

    timezone = settings.TIME_ZONE
    product_id = settings.ICAL_PRODID
    file_name = settings.ICAL_FILENAME

    def items(self):
        """Fetch events, oldest first"""
        return Event.objects.all().order_by("start_date")

    def item_guid(self, item):  # UID
        return item.uid

    def item_title(self, item):  # SUMMARY
        return item.title

    def item_description(self, item):  # DESCRIPTION
        return item.description

    def item_start_datetime(self, item):  # DTSTART
        return item.start_date

    def item_end_datetime(self, item):  # DTEND
        return item.end_date

    def item_location(self, item):  # LOCATION
        return item.location

    def item_link(self, item):  # URL
        url = f"/events/{item.uid}/"
        return url


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split("-"))
        return datetime.date(year, month, day=1)
    return datetime.datetime.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - datetime.timedelta(days=1)
    month = "month=" + str(prev_month.year) + "-" + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + datetime.timedelta(days=1)
    month = "month=" + str(next_month.year) + "-" + str(next_month.month)
    return month
