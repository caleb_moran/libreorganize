from django.test import TestCase
from django.urls import resolve


class WikisUrlsCase(TestCase):
    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/polls/create/").view_name, "polls:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/polls/1/").view_name, "polls:detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/polls/1/edit/").view_name, "polls:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/polls/1/delete/").view_name, "polls:delete")

    def test_public_list(self):
        """Check public list URL correct"""
        self.assertEqual(resolve("/polls/list/").view_name, "polls:public_list")

    def test_normal(self):
        """Check normal vote URL correct"""
        self.assertEqual(resolve("/polls/1/vote/").view_name, "polls:vote")

    def test_ranked(self):
        """Check ranked vote URL correct"""
        self.assertEqual(resolve("/polls/1/voterank/").view_name, "polls:voterank")

    def test_results(self):
        """Check ranked vote URL correct"""
        self.assertEqual(resolve("/polls/1/results/").view_name, "polls:results")
