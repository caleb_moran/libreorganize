from django.test import TestCase
from apps.polls.apps import PollsConfig


class PollsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(PollsConfig.name, "apps.polls")
