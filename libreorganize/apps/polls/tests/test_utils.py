import unittest
import numpy as np
from apps.polls.utils import Election


class TestVote(unittest.TestCase):
    def test_vote_per_candidate_ids(self):

        election = Election(candidates=[0, 1, 2, 3])

        election.vote([2, 1, 3])
        election.vote([2, 0, 3])
        election.vote([0, 1, 2])
        election.vote([0, 2, 1])
        election.vote([2, 1, 0])

        self.assertEquals(election.tally(), 2)

    def test_tally(self):

        election = Election(candidates=[0, 1, 2, 3])

        # Vote per candidate ids
        election.vote([2, 1, 3, 0])
        election.vote([1, 2, 3, 0])

        self.assertEqual(1 <= election.tally() <= 2, True)

    def test_read_from_list(self):

        election = Election(candidates=[0, 1, 2, 3])

        np.testing.assert_array_equal([2, 0, 1, 3], election.read_vote_from_list([2, 0, 1, 3]))

        # No duplicate rankings
        with self.assertRaises(ValueError):
            election.read_vote_from_list([0, 1, 2, 1])

        # Candidate IDs have to match
        with self.assertRaises(ValueError):
            election.read_vote_from_list([3, 1, 2, 4, 0])

    def test_add_list_vote(self):

        election = Election(candidates=[0, 1, 2, 3])

        # Vote per candidate ids
        election.vote([2, 0, 3, 1])
        election.vote([0, 2, 1])  # means a, c, b

        np.testing.assert_array_equal(election.votes, [[2, 0, 3, 1], [0, 2, 1]])

    def test_remove_candidate_from_vote(self):

        election = Election(candidates=[0, 1, 2, 3])

        self.assertListEqual(election.remove_candidate_from_vote([0, 1, 2, 3], 1), [0, 2, 3])

        self.assertListEqual(election.remove_candidate_from_vote([0, 1, 2, 3], 5), [0, 1, 2, 3])


class TestCounts(unittest.TestCase):
    def setUp(self):

        self.election = Election(candidates=[0, 1, 2, 3])

        self.votes = [
            [2, 1, 3],
            [2, 0, 3],
            [0, 1, 2],
            [0, 2, 1],
            [2, 1, 3],
        ]

    def test_eliminate_candidate(self):

        np.testing.assert_array_equal(
            self.election.eliminate_candidate(self.votes, 2), [[1, 3], [0, 3], [0, 1], [0, 1], [1, 3],]
        )

    def test_get_candidate_to_eliminate(self):
        self.votes = [
            [2, 1, 3],
            [2, 0, 3],
            [0, 1, 2],
            [0, 2, 1],
            [2, 1, 3],
        ]

        counts = self.election.count_first_ranked(self.votes)
        lowest_candidates = self.election.get_candidates_to_eliminate(counts)
        np.testing.assert_array_equal(lowest_candidates, [1, 3])

    def test_count_first_ranked(self):

        frequencies = self.election.count_first_ranked(self.votes)

        self.assertEqual(type(frequencies), dict)

        self.assertIn(0, frequencies)
        self.assertIn(1, frequencies)
        self.assertIn(2, frequencies)
        self.assertIn(3, frequencies)

        self.assertEqual(frequencies[0], 2)
        self.assertEqual(frequencies[1], 0)
        self.assertEqual(frequencies[2], 3)
        self.assertEqual(frequencies[3], 0)

    def test_has_winner(self):

        self.assertFalse(self.election.has_winner({0: 10, 1: 2, 2: 10, 3: 3}))

        # No absolute majority given
        self.assertFalse(self.election.has_winner({0: 11, 1: 2, 2: 10, 3: 3}))

        # No absolute majority given
        self.assertTrue(self.election.has_winner({0: 11, 1: 2, 2: 5, 3: 3}))

    def test_get_winner(self):

        winner = self.election.get_winner({0: 11, 1: 2, 2: 5, 3: 3})

        self.assertEqual(0, winner)

        with self.assertRaises(Exception):
            self.election.get_winner({0: 10, 1: 2, 2: 10, 3: 3})
