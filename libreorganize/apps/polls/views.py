import datetime
from random import randint
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, ListView as GenericListView
from django.forms import inlineformset_factory
from django.db.models import Count
from django.contrib import messages
from django.db import transaction

from core.mixins import AccessRestrictedMixin
from core.views import LibreCreateView, LibreDetailView, LibreUpdateView, LibreDeleteView

from apps.polls.models import Poll, PollChoice, PollDecision, PollVote
from apps.polls.forms import PollForm, PollChoicesFormSet, PollChoiceModelForm, PollDecisionForm, PollRankedDecisionForm
from apps.polls.utils import Election


class PollListView(AccessRestrictedMixin, GenericListView):
    permissions = ("polls.list_polls",)
    model = Poll


class PollDetailView(AccessRestrictedMixin, LibreDetailView):
    permissions = ("polls.detail_polls",)
    model = Poll
    pk_url_kwarg = "uid"


class PollDeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("polls.delete_polls",)
    model = Poll
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("polls:list")
    success_message = _("The poll has been deleted.")


class PollCreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("polls.create_polls",)
    model = Poll
    form_class = PollForm
    success_url = "/polls/"
    success_message = _("The poll has been created.")
    choices_formset_factory = inlineformset_factory(
        parent_model=Poll,
        model=PollChoice,
        form=PollChoiceModelForm,
        formset=PollChoicesFormSet,
        can_order=False,
        can_delete=False,
    )

    def get(self, request, *args, **kwargs):
        """Overriding get method to handle inline formset."""
        self.object = None
        form = self.get_form(self.get_form_class())
        formset = self.choices_formset_factory()
        return self.render_to_response(self.get_context_data(form=form, choices=formset))

    def post(self, request, *args, **kwargs):
        """Overriding post method to handle inline formsets."""
        self.object = None
        form = self.get_form(self.get_form_class())
        formset = self.choices_formset_factory(self.request.POST)

        if form.is_valid():
            self.object = form.save(commit=False)
            formset = self.choices_formset_factory(self.request.POST, instance=self.object)
            if formset.is_valid():
                self.object.save()
                formset.save()
                return HttpResponseRedirect(self.get_success_url())

        return self.render_to_response(self.get_context_data(form=form, choices=formset))


class PollEditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("polls.edit_polls",)
    model = Poll
    form_class = PollForm
    pk_url_kwarg = "uid"
    success_url = "/polls/"
    success_message = _("The poll has been edited.")
    choices_formset_factory = inlineformset_factory(
        parent_model=Poll,
        model=PollChoice,
        form=PollChoiceModelForm,
        formset=PollChoicesFormSet,
        extra=1,
        can_order=False,
        can_delete=True,
    )

    def get(self, request, *args, **kwargs):
        """Overriding get method to handle inline formset."""
        self.object = self.get_object()
        if self.object.get_poll_status() != "pending":
            messages.error(request, "You cannot edit a poll after it has started.")
            return HttpResponseRedirect(self.get_success_url())
        form = self.get_form(self.get_form_class())
        formset = self.choices_formset_factory(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, choices=formset))

    def post(self, request, *args, **kwargs):
        """Overriding post method to handle inline formsets."""
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        formset = self.choices_formset_factory(self.request.POST, instance=self.object)

        if form.is_valid() and formset.is_valid():
            result = super().form_valid(form)
            formset.save()
            return result
        else:
            return self.render_to_response(self.get_context_data(form=form, choices=formset))


class PollPublicListView(GenericListView):
    model = Poll
    template_name = "polls/poll_votinglist.html"
    ordering = ["-start_date"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["date"] = datetime.datetime.now()
        return context


class PollVoteView(AccessRestrictedMixin, FormView):
    form_class = PollDecisionForm
    success_url = "/polls/list"
    poll = None
    decision = None

    def load_poll(self, poll_id):
        self.poll = Poll.objects.get(pk=poll_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.decision
        context["poll"] = self.poll
        return context

    def dispatch(self, request, *args, **kwargs):
        self.load_poll(kwargs["poll_id"])
        if self.poll.get_poll_status() != "open":
            messages.error(request, "The poll is not open for voting.")
            return HttpResponseRedirect(self.get_success_url())
        elif not self.request.user.is_authenticated:
            return HttpResponseRedirect("/accounts/login/")

        user = self.request.user
        if user.polls.filter(uid=self.poll.uid).exists():
            messages.error(request, "You have already voted!")
            return HttpResponseRedirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        with transaction.atomic():
            # Save our vote
            instance = PollDecision()
            instance.poll = self.poll
            instance.choice = PollChoice.objects.get(pk=form.cleaned_data["choice_uid"])
            instance.save()
            # Save that we voted
            self.request.user.polls.add(self.poll)
            self.request.user.save()

        return super().form_valid(form)

    def get_template_names(self):
        if self.poll.poll_type == "ranked":
            return ["polls/polldecision_rank.html"]
        else:
            return ["polls/polldecision_normal.html"]


class PollVoteRankedView(AccessRestrictedMixin, FormView):
    form_class = PollRankedDecisionForm
    success_url = "/polls/list/"
    poll = None
    template_name = "polls/polldecision_rank.html"

    def load_poll(self, poll_id):
        self.poll = Poll.objects.get(pk=poll_id)

    def dispatch(self, request, *args, **kwargs):
        self.load_poll(kwargs["poll_id"])
        if self.poll.get_poll_status() != "open":
            messages.error(request, "The poll is not open for voting.")
            return HttpResponseRedirect(self.get_success_url())
        elif not self.request.user.is_authenticated:
            return HttpResponseRedirect("/accounts/login/")

        user = self.request.user
        if user.polls.filter(uid=self.poll.uid).exists():
            messages.error(request, "You have already voted!")
            return HttpResponseRedirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            # Save that we voted
            self.request.user.polls.add(self.poll)
            self.request.user.save()

            # Create a vote
            vote = PollVote()
            vote.save()

            # Save our candidate decisions
            for r in range(PollChoice.objects.filter(poll=self.poll).count()):
                instance = PollDecision()
                instance.poll = self.poll
                instance.choice = PollChoice.objects.get(pk=request.POST.get("result-" + str(r)))
                instance.rank = r
                instance.vote = vote
                instance.save()

        return super().post(request, *args, **kwargs)


class PollResultView(LibreDetailView):
    model = Poll
    pk_url_kwarg = "poll_id"
    template_name_suffix = "_results"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.get_poll_status() != "closed":
            messages.error(request, "The poll is not yet closed.")
            return HttpResponseRedirect("/polls/list/")
        return super().get(request, *args, **kwargs)

    def get_normal_results(self, poll):
        # Get a set of results [ { choice1, total1 }, { choice2, total2 }, ...]
        results = (
            PollDecision.objects.filter(poll=poll)
            .values_list("choice__name")
            .annotate(total=Count("choice__name"))
            .order_by("-total")
        )

        # If the winner is not already saved, make sure to save it
        if not poll.winner:
            winners = []

            if len(results) > 0:
                # Always append the first vote to the winners
                winners.append(results[0][0])

                # If a tie exists, append all tied votes
                if len(results) > 1 and results[0][1] == results[1][1]:
                    for r in range(len(results) - 1):
                        if results[0][1] == results[r + 1][1]:
                            winners.append(results[r + 1][0])
            else:
                # If no one voted, pick among the decisions randomly
                for choice in PollChoice.objects.filter(poll=poll):
                    winners.append(choice.name)

            # If we have a tie
            if len(winners) > 1:
                coinflip = randint(0, len(winners) - 1)
                winner_pk = winners[coinflip]
            else:
                winner_pk = winners[0]

            winner = PollChoice.objects.get(poll=poll, name=winner_pk)
            poll.winner = winner
            poll.save()

        return results

    def get_ranked_results(self, poll):
        # Check if a stored winner exists.  If so, return that object
        if poll.winner:
            return poll.winner

        # Compute the winner
        decisions = PollDecision.objects.filter(poll=poll).values_list("vote", "choice__uid").order_by("vote", "rank")
        choices = PollChoice.objects.filter(poll=poll).values_list("uid")
        choicelist = [item for t in choices for item in t]
        election = Election(choicelist)

        vote = None
        voter_votes = None
        for decision in decisions:
            # When we changed voters - submit this vote and restart the vote collection
            if vote != decision[0]:
                if voter_votes:
                    election.vote(voter_votes)
                vote = decision[0]
                voter_votes = []

            # Append the next decision to my voter list
            voter_votes.append(decision[1])

        # Ensure the last vote counts!
        if voter_votes:
            election.vote(voter_votes)

        # Save this result and load it for next time!
        winner = election.tally()
        result = PollChoice.objects.get(pk=winner)
        poll.winner = result
        poll.save()

        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["poll"] = self.object

        if self.object.poll_type == "normal":
            context["results"] = self.get_normal_results(self.object)
        else:
            context["result"] = self.get_ranked_results(self.object)
        return context
