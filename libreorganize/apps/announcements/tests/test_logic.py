from django.test import TestCase
from apps.accounts.models import Account
from apps.announcements.models import Announcement


class TestListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.announcement1 = Announcement.objects.get(uid=1)
        self.announcement2 = Announcement.objects.get(uid=2)


class TestCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "This is a test", "content": "abc abc testing", "date": "2020-03-01 11:45", "author": "William Walker"},
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.post("/announcements/create/", follow=True)
        self.assertTemplateUsed(response, template_name="announcements/announcement_form.html")


class TestDetailView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_announcement_detail_page(self):
        """Bring to Detail Page"""
        self.client.force_login(self.superuser)
        response = self.client.get("/announcements/1/", follow=True)
        self.assertContains(response, "title")


class TestEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/1/edit/",
            {"title": "This is an edited test", "content": "abc abc testing", "date": "2020-03-01 11:45", "author": "William Walker"},
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully edited.")


class TestDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "This is test", "content": "abc abc testing", "date": "2020-03-01 11:45", "author": "William Walker"},
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")
        response = self.client.post("/announcements/1/delete/?next=/announcements/", follow=True,)
        self.assertContains(response, "The announcement has been successfully deleted.")