from django.db import models
from django.urls import reverse

from apps.accounts.models import Account

class Announcement(models.Model):
    uid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=42)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)

    class Meta:
        default_permissions = ()
        permissions = (
            ("create_announcements", "Create announcements"),
            ("edit_announcements", "Edit announcements"),
            ("delete_announcements", "Delete announcements"),
        )

    def get_absolute_url(self):
        return reverse('announcements:detail', kwargs={'uid': self.uid})