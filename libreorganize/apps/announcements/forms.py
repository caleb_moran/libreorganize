from django import forms
from django_ckeditor_5.fields import CKEditor5Widget

from apps.announcements.models import Announcement


class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = Announcement
        fields = ("title", "content")
        widgets = {"content": CKEditor5Widget(config_name="wiki")}