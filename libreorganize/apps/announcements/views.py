from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from core.mixins import AccessRestrictedMixin

from apps.announcements.models import Announcement
from apps.announcements.forms import AnnouncementForm
from core.views import LibreCreateView, LibreUpdateView, LibreDetailView, LibreDeleteView

class ListView(ListView):
    model = Announcement
    pk_url_kwarg = "uid"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["announcement"] = Announcement.objects.all().order_by("-date")
        return context

class DetailView(LibreDetailView):
    model = Announcement
    pk_url_kwarg = "uid"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["announcements"] = Announcement.objects.all()
        return context


class CreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("announcements.create_announcements",)
    success_url = reverse_lazy("announcements:list")
    form_class = AnnouncementForm
    model = Announcement
    pk_url_kwarg = "uid"
    success_message = _("The announcement has been successfully created.")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)



class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("announcements.delete_announcements",)
    success_url = reverse_lazy("announcements:list")
    form_class = AnnouncementForm
    model = Announcement
    pk_url_kwarg = "uid"
    success_message = _("The announcement has been successfully deleted.")


class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("announcements.edit_announcements",)
    success_url = reverse_lazy("announcements:list")
    form_class = AnnouncementForm
    model = Announcement
    pk_url_kwarg = "uid"
    success_message = _("The announcement has been successfully edited.")
