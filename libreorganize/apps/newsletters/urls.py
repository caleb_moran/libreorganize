from django.urls import path
from apps.newsletters import views

app_name = "apps.newsletters"

urlpatterns = [
    path("", views.SignUpListView.as_view(), name="list"),
    path("create/", views.SignUpCreateView.as_view(), name="create"),
    path("<int:pk>/edit/", views.SignUpEditView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.SignUpDeleteView.as_view(), name="delete"),
]
