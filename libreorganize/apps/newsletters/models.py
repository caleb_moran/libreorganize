from django.db import models
from django.conf import settings


class SignUp(models.Model):
    email = models.EmailField(unique=True, error_messages={"unique": "This email is already subscribed."})
    full_name = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    language = models.CharField(max_length=16, choices=settings.LANGUAGES, default="en")

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_signups", "List Newsletters"),
            ("edit_signup", "Edit Newsletters"),
            ("delete_signup", "Delete Newsletters"),
        )

    def __str__(self):
        return self.email
