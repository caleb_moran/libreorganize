from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from core.mixins import AccessRestrictedMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect

from apps.newsletters.models import SignUp
from apps.newsletters.forms import SignUpForm
from core.views import LibreCreateView, LibreUpdateView, LibreDeleteView


class SignUpListView(AccessRestrictedMixin, ListView):
    permissions = "newsletters.list_signups"
    model = SignUp
    template_name = "newsletters/signup_list.html"
    context_object_name = "signups"
    ordering = ["-timestamp"]

    def get_context_data(self, *args, **kwargs):
        context = super(SignUpListView, self).get_context_data(**kwargs)
        context["emails"] = SignUp.objects.all()
        context["emailsen"] = SignUp.objects.filter(language="en")
        context["emailses"] = SignUp.objects.filter(language="es")
        return context


class SignUpCreateView(AccessRestrictedMixin, LibreCreateView):
    superuser = True
    form_class = SignUpForm
    template_name = "newsletters/signup_create.html"
    success_message = _("The newsletter has been successfully created.")

    def post(self, request, *args, **kwargs):
        form = SignUpForm(request.POST)
        if form.is_valid():
            signup = form.save(commit=False)
            signup.is_superuser = False
            signup.save()

            html = render_to_string(
                "email.html",
                {"message": f"{signup.full_name}, You have been subscribed to {settings.SITE_NAME} Newsletters",},
            )
            send_mail(
                subject=f"Welcome to | {settings.SITE_NAME}",
                message=html,
                html_message=html,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=(signup.email,),
                fail_silently=True,
            )
            messages.success(self.request, self.success_message)
            return HttpResponseRedirect(reverse("newsletters:list"))
        return render(request=request, template_name="newsletters/signup_create.html", context={"form": form})


class SignUpEditView(AccessRestrictedMixin, LibreUpdateView):
    superuser = True
    permissions = "newsletters.edit_signup"
    model = SignUp
    form_class = SignUpForm
    success_url = reverse_lazy("newsletters:list")
    success_message = _("The email has been successfully edited.")


class SignUpDeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = "newsletters.delete_signups"
    model = SignUp
    success_url = reverse_lazy("newsletters:list")
    success_message = _("The email has been successfully deleted.")
