import os
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.urls import path, include

urlpatterns = []

if os.path.exists(os.path.join(settings.BASE_DIR, "theme")):
    from theme.urls import *

urlpatterns += [
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
    path("i18n/", include("django.conf.urls.i18n")),
    path("accounts/", include("apps.accounts.urls", namespace="accounts")),
    path("boxes/", include("apps.boxes.urls", namespace="boxes")),
    path("events/", include("apps.events.urls", namespace="events")),
    path("memberships/", include("apps.memberships.urls", namespace="memberships")),
    path("wikis/", include("apps.wikis.urls", namespace="wikis")),
    path("polls/", include("apps.polls.urls", namespace="polls")),
    path("newsletters/", include("apps.newsletters.urls", namespace="newsletters")),
    path("announcements/", include("apps.announcements.urls", namespace="announcements")),
    path("ckeditor5/", include("django_ckeditor_5.urls")),
    path("authentication/", include("social_django.urls", namespace="authentication")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
