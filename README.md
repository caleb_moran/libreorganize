# Status

Branch | Pipeline | Coverage
--- | --- | ---
release | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/release/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/release) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/release/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/release)
master | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/master/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/master) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/master/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/master)

# About
- LibreOrganize is a web platform for local unions and progressive organizations.
- We are helping build popular power through democratic control of our data.


# Installation

## LibreOrganize

*Note: If you are following these instructions when deploying the website, it is **highly** recommended that you clone the repository in `/srv`.*

1. Clone this repository.

2. Change the directory to `libreorganize` using `$ cd libreorganize/`.

3. Create a virtual environment using `$ python3 -m venv venv`.

4. Activate the virtual environment using `$ source venv/bin/activate`.

5. Install the dependencies using `$ pip install -r requirements.txt`.
  - *Note: You can safely ignore any errors about `bdist_wheel`.*

6. Change the directory to `libreorganize` using `$ cd libreorganize/`.

7. Apply the migrations using `$ python manage.py migrate`.

8. Create a superuser account using `$ python manage.py createsuperuser`.

9. Load the initial data using `$ python manage.py loaddata core/fixtures/initial_data.json`.

You should now have a development version of the `libreorganize` accessible at `localhost:8000` or `127.0.0.1:8000`.

## Theme

*Follow these instructions only if you want to install a custom theme.*

1. Change the directory to `libreorganize` using `$ cd libreorganize/`.

2. Clone the theme repository using `$ git clone git@gitlab.com:libreorganize/libreorganize-custom-themes.git theme/`.

3. Edit the file `settings.py` and apply your custom settings.

4. If applicable, edit the file `urls.py` and create URLs for your theme.

# Development

*Always activate the virtual environment before performing operations.*

- Run the server using `$ python manage.py runserver`.

- Stop the server by pressing `Ctrl-C`.

# Testing

*Always activate the virtual environment before performing operations.*

*Automated tests are supported only on Unix-like Operating Systems.*

1. Change the directory to `libreorganize` using `$ cd libreorganize/`.

2. Run Functional Tests using `$ source tests/automation/run_func_tests.sh`.

3. Run Unit Tests using `$ source tests/automation/run_unit_tests.sh`.

4. The code coverage will be printed after the execution of all Unit Tests.

## Deployment

*Follow the installation instructions before continuing. If you are running the Django server, press Control-C to close it.*

*Make sure you have `root` privileges.*

### Django configuration

1. Collect the static files (by default in `/var/www/libreorganize/`) using `$ python manage.py collectstatic`.

2. Generate a `SECRET_KEY` using `$ python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'`.

3. Copy the returned key to your clipboard because we will need it soon.

4. Open `.env` for editing using your preferred text editor.

5. Replace the current `SECRET_KEY` with the one you generated earlier (it's in your clipboard, right!?).

6. Configure other variables to your liking.

7. Save and exit.

### Firewall configuration
**If you have a firewall set up (recommended), make sure to open ports 80 and 443.**

- If you have `UFW` set up:
  - Run `$ ufw allow http`.
  - Run `$ ufw allow https`.

- If don't have `UFW` and have only `iptables` set up:
  - Open `/etc/sysconfig/iptables` for editing using your preferred text editor.
  - Add the following lines to the file if they do not already exist, then save and exit:
```
-A INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -p tcp --dport 443 -j ACCEPT
```

### Installing additional dependencies
- Install Gunicorn using `$ pip install gunicorn`.
- Install Nginx using `$ apt install nginx`.
- Install Certbot using `$ apt install python3-certbot-nginx`.

### Setting up the Gunicorn service
- Open `/etc/systemd/system/gunicorn_lo.socket` for editing using your preferred text editor.
- Add the following to the file, then save and exit:
```
[Unit]
Description=gunicorn libreorganize socket

[Socket]
ListenStream=/run/gunicorn_lo.sock

[Install]
WantedBy=sockets.target
```
- Open `/etc/systemd/system/gunicorn_lo.service` for editing using your preferred text editor.
- Add the following to the file:
```
[Unit]
Description=gunicorn blog daemon
Requires=gunicorn_lo.socket
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/srv/libreorganize
ExecStart=/srv/libreorganize/venv/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn_lo.sock \
          core.wsgi:application

[Install]
WantedBy=multi-user.target
```
- Start the Gunicorn socket using `$ systemctl start gunicorn_lo.socket`.
- Enable the Gunicorn socket (run at startup) using `$ systemctl enable gunicorn_lo.socket`.

### Setting up Nginx
- Remove the `default` configuration from `sites-enabled` using `$ rm /etc/nginx/sites-enabled/default`.
- Open `/etc/nginx/sites-available/blog` for editing using your preferred text editor.
- Add the following to the file, then save and exit:
```
server {
    listen 80;
    server_name YOUR_FULLY_QUALIFIED_DOMAIN_NAME;

    location = /favicon.ico { access_log off; log_not_found off; }

    location /static/ {
        root /var/www/libreorganize;
    }

    location /media/ {
        root /var/www/libreorganize;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn_lo.sock;
    }
}
```
- Activate the Nginx config using `$ ln -s /etc/nginx/sites-available/libreorganize /etc/nginx/sites-enabled/libreorganize`.
- Restart Nginx `$ systemctl restart nginx`.

### Setting up the HTTPS certificate
- Create an HTTPS certificate with Certbot using `$ certbot --nginx -d YOUR_FULLY_QUALIFIED_DOMAIN_NAME`.
- Follow the script instructions.
- You should choose option 2 (Redirect) when the script asks if you want users to be redirected to the `HTTPS` version of the website if they try accessing the `HTTP` version.

Good job! You should now have a running instance of the blog.

### About SMTP

```
SMTPServerDisconnected at /
please run connect() first
```

If you encounter the error above, you did not properly set up SMTP.
There are 2 easy solutions you can follow:

- Fill out the email variables in `.env`.
- Output the email content to the `Terminal` by changing the `EMAIL_BACKEND` variable in the settings to the following:

`EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'`.

### Docker Support

In order to get a docker container of LibreOrganize you need the following commands...

```
docker build --tag libreorganize:latest .
docker run --publish 8000:8000 -it libreorganize python /code/manage.py runserver 0.0.0.0:8000
```

### Cron job

Some apps such as *memberships* may require a cron job that runs behind the scenes to keep things updated. If you plan on using the memberships app please follow the following steps.

```
$ crontab -e
m h dom mon dow command
0 0 * * * cd /srv/libreorganize/libreorganize/  ../venv/bin/python manage.py update_membership_status
```
